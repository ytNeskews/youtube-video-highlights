import _ from 'lodash';

/**
 * groups the hightlights
 * e.g.:
 * input: [{ id: 1, value: 'a' }, { id: 1, value: 'b' }, { id: 2, value: 'c' }, { id: 2, value: 'd' }]
 * output: [ { id: 1, values: [ 'a', 'b' ] }, { id: 2, values: [ 'c', 'd' ] } ]
 */
export function group({ highlights }) {
	// get an array of all the highlight-ids we have
	const ids = _.uniq(highlights.map(highlight => highlight.highlightId));

	const grouped = [];
	for (const theId of ids) {
		// we only want the one id we need for this iteration
		const acc = highlights.filter(highlight => highlight.highlightId === theId);

		// filter for the ids => we only want ONE id for all its videos
		const { highlightId } = acc[0];

		// this is an id we will need as key
		const { idHighlight } = acc[0];

		// the id of the customer the highlight belongs to
		const { idCustomers } = acc[0];

		// get the videoIds
		const videoIds = acc.map(highlight => highlight.videoId);

		// and finally push it to the groups
		grouped.push({ idHighlight, highlightId, idCustomers, videoIds });
	}

	return grouped;
}

/**
 * Get the correct player in our array of players
 * @param { String } idHighlight - the id for the highlight we want
 * @param { Array } highlights - all the highlights we got from DB
 * @returns the corresponding highlight
 */
export function getPlayerByHighlightId({ selectedHighlight, highlights }) {
	return highlights.filter(
		highlight => highlight.highlightId === selectedHighlight
	)[0];
}

export function getVideoIdsByHighlightId({ highlights, highlightId }) {
	if (!highlightId || typeof highlightId !== 'number')
		throw new Error(
			`expected a highlightId as number, but got: ${highlightId}`
		);
	if (!highlights || highlights.length < 1)
		throw new Error(`expected an array of highlights, but got: ${highlights}`);

	return highlights
		.filter(highlight => highlight.highlightId === highlightId)
		.map(highlight => highlight.videoId)
		.filter(videoId => videoId);
}

/**
 * Alters the array so that it does only contain unique entries
 * @param { Array } array - the array which should only contain unique elements
 * @returns the unique array
 */
export function uniq({ array = [] }) {
	if (array.length < 1) return array;
	return Array.from(new Set(array));
}

export default {
	getPlayerByHighlightId,
	getVideoIdsByHighlightId,
	group
};
