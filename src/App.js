import React from 'react';
import MainHighlightScreen from './components/Highlights/MainHighlightScreen';

export default class App extends React.Component {
	render() {
		return <MainHighlightScreen />;
	}
}
