import callAdd from './server/callAdd';

/**
 * add a video to database
 * @param { String } videoIdToAdd - the id of the video we want to add
 * @param { Integer } highlightId - the id of the highlight we want to update
 * @returns the response object
 */
export default async function addVideo({ videoIdToAdd, highlightId }) {
	return await callAdd({ endpoint: 'add', videoIdToAdd, highlightId });
}
