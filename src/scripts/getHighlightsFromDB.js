import callGetHighlights from './server/callGetHighlights';

/**
 * Get the highlights from the database
 */
export default async function getHighlightsFromDB() {
	// get the videos from db
	const response = await callGetHighlights({ endpoint: 'getHighlights' }); // response body
	const { body } = response;
	if (body.errno === 'ECONNREFUSED')
		throw new Error('Could not connect to database');
	if (body.errno) throw new Error(`${body.sqlMessage}`);

	return body;
}
