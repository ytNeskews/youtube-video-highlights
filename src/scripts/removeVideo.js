import callRemove from './server/callRemove';

/**
 * remove a video from the hightlight
 * @param { string } videoId - the video id of the video which we want to remove
 * @param { string } highlightId - the highlight from which we want to remove the video
 */
export default async function removeVideo({ videoIdToRemove, highlightId }) {
	const response = await callRemove({
		endpoint: 'remove',
		videoIdToRemove,
		highlightId
	});
	return response.body;
}
