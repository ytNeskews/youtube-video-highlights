import callGet from './server/callGet';

/**
 * get the videos we find on db to display them
 */
export default async function getVideos() {
	const response = await callGet({ endpoint: 'connect' });
	return response.body;
}
