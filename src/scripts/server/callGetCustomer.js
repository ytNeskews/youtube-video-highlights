import request from 'superagent';
import SERVER_URL from '../../serverUrl';

/**
 * invokes a http get to the server
 * @param { string } endpoint - the endpoint on the server
 * @param { string } name - the unique name of a customer
 */
export default async function callGetCustomer({ endpoint, name }) {
	return await request
		.get(`${SERVER_URL}${endpoint}?name=${name}`)
		.catch(error => {
			throw new Error(error);
		});
}
