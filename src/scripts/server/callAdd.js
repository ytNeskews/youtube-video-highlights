import request from 'superagent';
import SERVER_URL from '../../serverUrl';

/**
 * invokes a http get to the server
 * @param { string } videoIdToAdd - the id of the video we want to add
 * @param { string } endpoint - the endpoint on the server
 */
export default async function callGet({ endpoint, videoIdToAdd, highlightId }) {
	const response = await request
		.get(
			`${SERVER_URL}${endpoint}?videoId=${videoIdToAdd}&highlightId=${highlightId}`
		)
		.catch(error => {
			throw new Error(error);
		});

	if (response.status !== 200) throw new Error('Could not add the video');

	return response;
}
