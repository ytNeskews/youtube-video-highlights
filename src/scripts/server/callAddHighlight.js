import request from 'superagent';
import SERVER_URL from '../../serverUrl';

/**
 * invokes a http get to the server
 */
export default async function callAddHighlight({ endpoint, idCustomers }) {
	const response = await request
		.get(`${SERVER_URL}${endpoint}?idCustomers=${idCustomers}`)
		.catch(error => {
			throw new Error(error);
		});

	if (response.status !== 200) throw new Error('Could not add the video');

	return response;
}
