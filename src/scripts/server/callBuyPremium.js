import request from 'superagent';
import SERVER_URL from '../../serverUrl';

/**
 * invokes a http get to the server
 * @param { string } name - the name of the customer we want to update to premium
 * @param { string } endpoint - the endpoint on the server
 */
export default async function callGet({ endpoint, name }) {
	const response = await request
		.get(`${SERVER_URL}${endpoint}?name=${name}`)
		.catch(error => {
			throw new Error(error);
		});

	if (response.status !== 200) throw new Error('Could not update the user');

	return response;
}
