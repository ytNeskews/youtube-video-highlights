import request from 'superagent';
import SERVER_URL from '../../serverUrl';

/**
 * invokes a http get to the server
 * @param { string } endpoint - the endpoint on the server
 */
export default async function callGet({ endpoint }) {
  return await request.get(`${SERVER_URL}${endpoint}`).catch(error => {
    throw new Error(error);
  });
}
