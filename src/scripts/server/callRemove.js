import request from 'superagent';
import SERVER_URL from '../../serverUrl';

/**
 * invokes a http get to the server
 * @param { string } endpoint - the endpoint on the server
 * @param { string } videoIdToRemove - the id of the video we want to remove
 */
export default async function callGet({ endpoint, videoIdToRemove, highlightId }) { 
  const response = await request
    .get(`${SERVER_URL}${endpoint}?videoId=${videoIdToRemove}&highlightId=${highlightId}`)
    .catch(error => {
      throw new Error(error);
    });

  if (response.status !== 200) throw new Error('Could not add the video');

  return response;
}
