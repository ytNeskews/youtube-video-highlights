import callAddHighlight from './server/callAddHighlight';

/**
 * add a highlight to database
 * @param { integer } idCustomers - the id of the customer in the db
 * @returns the response object
 */
export default async function addHighlightToDB({ idCustomers }) {
	return await callAddHighlight({ endpoint: 'addHighlight', idCustomers });
}
