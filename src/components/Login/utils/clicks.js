import login from './login';

const LOGIN_STORAGE = window.localStorage;

// TODO: remove all that location reloading by replacing with setState expressions in parent components

export async function handleLoginClick(event) {
	// the user name which was requested to log in.
	const customerName = event.target.value;

	// the customer which logged in
	const customer = await login({ name: customerName });

	if (!customer)
		this.setState({
			errorMessage: `There is no user account with name ${customerName}`
		});

	const { name } = customer; // the unqiue name of the customer

	window.location.reload();
	LOGIN_STORAGE.setItem('name', name);
}
