import callGetCustomer from '../../../scripts/server/callGetCustomer';

/**
 * @param { string } name - the unique name of a customer.
 * @returns { object } - an object with information about the requested customer
 */
export default async function login({ name }) {
	const response = await callGetCustomer({ endpoint: 'getCustomer', name });

	const customers = response.body; // the customers matching the pattern

	return customers[0]; // we only want the customer at first position
}
