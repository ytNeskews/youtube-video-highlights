import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, FormControl } from 'react-bootstrap';

import ErrorAlerter from '../Hints/ErrorAlerter';

import login from './utils/login';

import STYLES from './styles';

const LOGIN_STORAGE = window.localStorage;

class LoginForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loginName: ''
		};

		this.handleUpdate = this.handleUpdate.bind(this);
		this.handleLoginClick = this.handleLoginClick.bind(this);
	}

	handleUpdate(event) {
		const { value } = event.target;
		this.setState({
			loginName: value
		});
	}

	async handleLoginClick(event) {
		// the user name which was requested to log in.
		const customerName = event.target.value;
		try {
			// the customer which logged in
			const customer = await login({ name: customerName });

			const { name, idCustomers } = customer; // the unqiue name of the customer

			LOGIN_STORAGE.setItem('name', name);
			LOGIN_STORAGE.setItem('idCustomers', idCustomers);
			window.location.reload();
		} catch (error) {
			this.setState({
				errorMessage: `We can't find a user called "${customerName}"`
			});
		}
	}

	render() {
		return (
			<div>
				{this.state.errorMessage && (
					<ErrorAlerter errorMessage={this.state.errorMessage} />
				)}
				<div style={STYLES.login}>
					<FormControl
						type="text"
						value={this.state.loginName}
						placeholder="Your account name"
						onChange={this.handleUpdate}
						style={STYLES.textArea}
					/>
					<Button
						bsStyle=""
						style={STYLES.button}
						onClick={this.handleLoginClick}
						value={this.state.loginName}
					>
						Login
					</Button>
				</div>
			</div>
		);
	}
}

LoginForm.propTypes = {
	handleLoginClick: PropTypes.func
};

export default LoginForm;
