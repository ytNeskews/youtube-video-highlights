import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Glyphicon } from 'react-bootstrap';

import STYLES from './styles';

class BuyAccountButton extends Component {
	render() {
		return (
			<div>
				<h3 style={{ marginTop: '10px' }}>or</h3>
				<Button
					bsStyle=""
					style={STYLES.buttonGetAccount}
					onClick={this.props.handleShow}
				>
					<Glyphicon glyph="bitcoin" style={STYLES.getAccount} />Get an account
				</Button>
			</div>
		);
	}
}

BuyAccountButton.propTypes = {
	handleShow: PropTypes.func
};

export default BuyAccountButton;
