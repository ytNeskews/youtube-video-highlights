export default {
	login: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'stretch'
	},
	button: {
		fontSize: '14px',
		height: '30px',
		borderRadius: '5px',
		color: 'rgba(20,20,20,0.9)',
		width: '20%'
	},
	buttonGetAccount: {
		fontSize: '14px',
		height: '30px',
		marginTop: '10px',
		borderRadius: '5px',
		color: 'rgba(20,20,20,0.9)'
	},
	textArea: {
		height: '30px',
		marginRight: '20px',
		fontSize: '14px',
		width: '50%'
	},
	getAccount: {
		marginRight: '10px'
	}
};
