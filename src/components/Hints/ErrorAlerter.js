import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

/**
 * This Error Alerter should be used to show up error messages in the GUI
 */
export default class ErrorAlerter extends React.PureComponent {
	render() {
		return (
			<Alert bsStyle="danger" onDismiss={this.handleDismiss}>
				<h2>Oh snap! You got an error!</h2>
				<h4>
					{typeof this.props.errorMessage === 'string'
						? this.props.errorMessage
						: 'Unknown Error'}
				</h4>
			</Alert>
		);
	}
}

ErrorAlerter.propTypes = {
	errorMessage: PropTypes.string
};
