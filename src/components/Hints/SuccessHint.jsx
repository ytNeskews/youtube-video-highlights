import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';

class SuccessHint extends Component {
	render() {
		return (
			<Alert
				bsStyle="success"
				style={{
					fontSize: '18px'
				}}
			>
				{
					/*eslint-disable react/prop-types*/
					this.props.children
				}
			</Alert>
		);
	}
}

export default SuccessHint;
