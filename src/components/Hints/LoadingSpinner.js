import React, { Component } from 'react';

class LoadingSpinner extends Component {
	render() {
		return (
			<i
				className="fa fa-circle-o-notch fa-spin"
				style={{ fontSize: '48px' }}
			/>
		);
	}
}

export default LoadingSpinner;
