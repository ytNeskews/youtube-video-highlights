import React, { Component } from 'react';
import { Image } from 'react-bootstrap';

class Logo extends Component {
	render() {
		return (
			<Image
				src={'logo_ereased.png'}
				style={{
					width: '200px',
					height: '200px',
					position: 'absolute',
					right: '24px',
					top: '0px'
				}}
			/>
		);
	}
}

export default Logo;
