import React, { Component } from 'react';

import Logo from './Logo';
import GetAccountBanner from '../Payment/GetAccountBanner';

import STYLES from './styles';

class Banner extends Component {
	render() {
		return (
			<h1 className="display-4 banner" style={STYLES.overlay}>
				<GetAccountBanner />

				<div style={STYLES.heading}>
					<div style={STYLES.message}>YouToo</div>
					<p>
						<small style={STYLES.subheading}>
							{' '}
							Watch videos the way <b style={STYLES.accentInSubheading}>
								YOU
							</b>{' '}
							want!
						</small>
					</p>
				</div>
				<Logo />
			</h1>
		);
	}
}

export default Banner;
