const STYLES = {
	heading: {
		backgroundColor: 'rgba(40,40,40,0.95)',
		borderRadius: '5px',
		width: '60%',
		marginLeft: '2%',
		color: 'white',
		fontSize: '18px',
		paddingBottom: '40px',
		paddingTop: '40px'
	},
	message: {
		fontSize: '80px',
		fontWeight: '400'
	},
	subheading: {
		color: '#F0F0F0'
	},
	accentInSubheading: {
		color: '#ff0000'
	},
	overlay: {
		position: 'relative',
		textAlign: 'center',
		display: 'flex',
		flexDirection: 'row',
		flexWrawp: 'wrap',
		height: '200px'
	}
};

export default STYLES;
