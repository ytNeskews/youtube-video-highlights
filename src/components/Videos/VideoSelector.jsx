import React from 'react';
import PropTypes from 'prop-types';
import {
	Modal,
	Button,
	Media,
	Glyphicon,
	FormGroup,
	FormControl
} from 'react-bootstrap';

export default class VideoSelector extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			showModal: false, // indicates if the modal needs to be shown
			videoIdToAdd: '' // the value of the idField, which indicates the video to add
		};

		this.handleShow = this.handleShow.bind(this);
		this.handleCancel = this.handleCancel.bind(this);
		this.renderAddVideoButton = this.renderAddVideoButton.bind(this);

		this.handleChange = this.handleChange.bind(this);
		this.getValidationState = this.getValidationState.bind(this);
		this.handleAddVideo = this.handleAddVideo.bind(this);
	}

	/**
	 * closes the video selector
	 */
	handleCancel() {
		this.setState({ showModal: false });
	}

	/**
	 * shows the video selector
	 */
	handleShow(event) {
		event.preventDefault();
		this.setState({ showModal: true });
	}

	/**
	 * renders the add button to the component
	 */
	renderAddVideoButton() {
		return (
			<div onClick={event => this.handleShow(event)}>
				<Media>
					<Button
						bsStyle="highlight"
						style={{
							backgroundColor: 'rgba(40,40,40,0.85)',
							color: 'white',
							width: '68%',
							fontSize: '14px',
							marginRight: '30%',
							marginBottom: '15px',
							marginLeft: '2%',
							marginTop: ' 20px'
						}}
					>
						<Glyphicon glyph="plus" />
					</Button>
				</Media>
			</div>
		);
	}

	/**
	 * changes the color of the idField to green, yellow or red
	 */
	getValidationState() {
		const { videoIdToAdd } = this.state;
		const length = videoIdToAdd && videoIdToAdd.length;
		if (length > 10) return 'success';
		else if (length > 5) return 'warning';
		else if (length > 0) return 'error';
		return null;
	}

	/**
	 * handles the change of the idField
	 */
	handleChange(event) {
		const { value } = event.target;
		const isUrl = /^(http|www)/i.test(value); // check if it is an url
		let videoIdToAdd; // the id of the video we want to add

		// the url will automatically reduced to the video url
		if (isUrl) {
			// the index in the string, where the id begins
			// example url: https://www.youtube.com/watch?v=D8YQn7o_AyA
			const startsAt = value.indexOf('v=') + 2;
			// the substring is the actual video id
			videoIdToAdd = value.substring(startsAt, value.length);
		} else videoIdToAdd = value;

		this.setState({ videoIdToAdd });
	}

	/**
	 * handles the add video event
	 */
	handleAddVideo() {
		this.handleCancel(); // close the modal
		// trigger the add video in the parent component
		this.props.handleAddVideo(this.state.videoIdToAdd);
	}

	/**
	 * renders the field, on which the user can insert a video id for a video he want
	 * to add
	 */
	renderIdField() {
		return (
			<FormGroup
				controlId="idField"
				validationState={this.getValidationState()}
			>
				<FormControl
					type="text"
					value={this.state.videoIdToAdd}
					placeholder="Enter a video url"
					onChange={this.handleChange}
					style={{ height: '50px' }}
				/>
			</FormGroup>
		);
	}

	/**
	 * if the component is mounted, it checks which video id is attempted to be
	 * added
	 */
	componentDidMount() {
		this.setState({
			videoToAdd: this.props.videoToAdd
		});
	}

	render() {
		return (
			<div>
				{this.renderAddVideoButton()}
				<Modal
					show={this.state.showModal}
					onHide={this.handleCancel}
					style={{
						// vertical centering the modal relative to the page
						display: 'flex',
						flexDirection: 'column',
						justifyContent: 'center'
					}}
				>
					<Modal.Header>
						<Modal.Title>
							<p style={{ fontSize: '20px' }}>Add a video to this Highlight</p>
						</Modal.Title>
					</Modal.Header>
					<Modal.Body style={{ margin: '0 auto' }}>
						{this.renderIdField()}
					</Modal.Body>
					<Modal.Body style={{ fontSize: '16px' }}>
						<p>
							Don&apos;t be puzzled, we will{' '}
							<strong>automatically extract the video id from the url</strong>{' '}
							:)
						</p>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={this.handleCancel}>Cancel</Button>
						<Button onClick={this.handleAddVideo}>Add</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}

VideoSelector.propTypes = {
	handleAddVideo: PropTypes.func,
	videoToAdd: PropTypes.string
};
