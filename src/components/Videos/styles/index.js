const STYLES = {
	videoPanel: {
		backgroundColor: 'rgba(40,40,40,0.95)',
		borderRadius: '5px',
		display: 'flex',
		flexDirection: 'column-reverse',
		width: '500px',
		height: '400px',
		paddingLeft: '18px',
		paddingTop: '18px',
		marginBottom: '20px',
		marginRight: '2%'
	},
	idField: {
		width: '95%',
		height: '95%'
	},
	removeButton: {
		width: '20%',
		fontSize: '18px',
		margin: '0 auto'
	}
};

export default STYLES;
