import React, { Component } from 'react';
import PropTypes from 'prop-types';

import RemoveButton from './Misc/Buttons/RemoveButton';
import VideoPanel from './Misc/VideoPanel';

import STYLES from './styles';

class PlayerElement extends Component {
	render() {
		return (
			<VideoPanel>
				<RemoveButton
					videoId={this.props.videoId}
					handleRemoveVideo={this.props.handleRemoveVideo}
				/>
				<div id={this.props.videoId} style={STYLES.idField} />
			</VideoPanel>
		);
	}
}

PlayerElement.propTypes = {
	videoId: PropTypes.string,
	handleRemoveVideo: PropTypes.func
};

export default PlayerElement;
