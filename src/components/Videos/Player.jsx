import React from 'react';
import YouTubePlayer from 'youtube-player';
import PropTypes from 'prop-types';
import { Alert, OverlayTrigger, Tooltip } from 'react-bootstrap';

import VideoSelector from './VideoSelector';
import ErrorAlerter from '../Hints/ErrorAlerter';
import LoadingSpinner from '../Hints/LoadingSpinner';
import BackButton from './Misc/Buttons/BackButton';
import PlayerElement from './PlayerElement';

import getHighlightsFromDB from '../../scripts/getHighlightsFromDB';
import { getVideoIdsByHighlightId } from '../../utils';
import addVideo from '../../scripts/addVideo';
import removeVideo from '../../scripts/removeVideo';

const styles = {
	plusButton: {
		fontSize: '24px',
		margin: '0px auto'
	}
};

const tooltip = <Tooltip id="tooltip">Go Back to Overview</Tooltip>;

export default class YouTube extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			videoIdToAdd: 'Select a Video',
			videoIds: []
		};

		this.handleRemoveVideo = this.handleRemoveVideo.bind(this);
		this.handleAddVideo = this.handleAddVideo.bind(this);
		this.renderNoVideosStatement = this.renderNoVideosStatement.bind(this);
	}

	/**
	 * changed the id of the video which will be removed
	 * @param { string } videoIdToRemove - the id of the video
	 */
	async handleRemoveVideo(videoIdToRemove) {
		try {
			this.setState({ videoIdToRemove });

			// the id of the highlight we want to update
			const { highlightId } = this.props;

			// wait for the video to be removed from the database
			await removeVideo({ videoIdToRemove, highlightId });
		} catch (error) {
			this.setState({
				errorMessage: error.message || 'Could not remove the video'
			});
		}
	}

	/**
	 * changed the id of the video which will be added
	 * @param { string } videoIdToAdd - the id of the video
	 */
	async handleAddVideo(videoIdToAdd) {
		try {
			// the id of the highlight we want to update
			const { highlightId } = this.props;

			// wait for the video to be added on the database
			await addVideo({ videoIdToAdd, highlightId });

			// force update after we added a video
			this.forceUpdate();

			// get the updated highlights from DB
			const highlights = await getHighlightsFromDB();
			// get the needed videoIds from the array of highlights
			const videoIds = getVideoIdsByHighlightId({ highlights, highlightId });

			// finally, update the state
			await this.setState({ videoIds });

			// after we await the update for the videoIds, trigger the video to be
			// actually inserted into the videoPanel we create in the step before
			this.setState({ videoIdToAdd });
		} catch (error) {
			this.setState({ errorMessage: error.message });
		}
	}

	/**
	 * BEFORE the player mounts, we fetch the videoIds of the VideoSelector
	 * we want to show
	 */
	async componentWillMount() {
		try {
			// get all the videos/players from the db
			const videoIds = this.props.videoIds.filter(videoId => videoId);

			this.setState({
				videoIds: videoIds.length > 0 ? videoIds : 'No Videos here yet!',
				errorMessage: null
			});
		} catch (error) {
			console.log(error);
			this.setState({ errorMessage: error.message });
		}
	}

	/**
	 * As soon as we have videoIds, we render the videos on the component
	 */
	componentDidMount() {
		this.state.videoIds.length > 0 &&
			this.setState({ createPlayerElements: true });
	}

	/**
	 * Give the user the information that we could not find a video for that hightlight
	 * @param { string } message - the message the user will see
	 */
	renderNoVideosStatement(message) {
		return (
			<Alert
				bsStyle="success"
				style={{
					fontSize: '18px'
				}}
			>
				{message}
			</Alert>
		);
	}

	render() {
		return (
			<div className="content">
				<div style={styles.plusButton}>
					{parseInt(this.props.idCustomers, 10) ===
						parseInt(window.localStorage.getItem('idCustomers'), 10) && (
						<VideoSelector
							createDropDown={this.createDropDown}
							videoIdToAdd={this.state.videoIdToAdd}
							handleAddVideo={this.handleAddVideo}
						/>
					)}
				</div>
				<main>
					<div className="bottomWrapper">
						<div className="players">
							{// if we have an error, print it
							this.state.errorMessage && (
								<ErrorAlerter errorMessage={this.state.errorMessage} />
							)}

							{// if we neither have an error or videoIds, we are in loading state
							!this.state.videoIds &&
								!this.state.errorMessage && <LoadingSpinner />}

							{// in case we have an array of players, create a PlayerElement
							this.state.videoIds &&
								this.state.videoIds.constructor === Array &&
								this.state.videoIds.map(videoId => {
									return (
										<PlayerElement
											key={videoId}
											videoId={videoId}
											handleRemoveVideo={this.handleRemoveVideo}
										/>
									);
								})}

							{// in case we have a PlayerElement, render the actual player on it
							this.state.videoIds &&
								this.state.videoIds.constructor === Array &&
								this.state.videoIds.forEach(videoId => {
									document.getElementById(videoId) &&
										YouTubePlayer(videoId, { videoId });
								})}

							{// in case we have videoIds, but it is not an array
							this.state.videoIds &&
								this.state.videoIds.constructor !== Array &&
								this.renderNoVideosStatement(this.state.videoIds)}
						</div>
					</div>
				</main>
				<OverlayTrigger placement="bottom" overlay={tooltip}>
					{/* Back to Overview Button */}
					<BackButton handleBack={this.props.handleBack} />
				</OverlayTrigger>
			</div>
		);
	}
}

YouTube.propTypes = {
	videoIds: PropTypes.array,
	idCustomers: PropTypes.string,
	handleBack: PropTypes.func,
	handleAddVideo: PropTypes.func,
	highlightId: PropTypes.number,
	getHighlightsFromDB: PropTypes.func
};
