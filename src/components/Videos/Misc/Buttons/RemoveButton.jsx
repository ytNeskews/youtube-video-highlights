import React, { Component } from 'react';
import {
	OverlayTrigger,
	Button,
	Glyphicon,
	ButtonToolbar
} from 'react-bootstrap';
import PropTypes from 'prop-types';

import tooltip from '../tooltip';
import STYLES from '../../styles';

class RemoveButton extends Component {
	render() {
		return (
			<ButtonToolbar>
				<OverlayTrigger placement="bottom" overlay={tooltip}>
					<Button
						style={STYLES.removeButton}
						onClick={() => {
							this.props.handleRemoveVideo(this.props.videoId);
						}}
					>
						<Glyphicon glyph="trash" />
					</Button>
				</OverlayTrigger>
			</ButtonToolbar>
		);
	}
}

RemoveButton.propTypes = {
	handleRemoveVideo: PropTypes.func,
	videoId: PropTypes.string
};

export default RemoveButton;
