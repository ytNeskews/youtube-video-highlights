import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Glyphicon } from 'react-bootstrap';

class BackButton extends Component {
	render() {
		return (
			<Button
				style={{
					backgroundColor: 'rgba(40,40,40,0.85)',
					color: 'white',
					width: '68%',
					fontSize: '14px',
					marginRight: '30%',
					marginBottom: '15px',
					marginLeft: '2%',
					marginTop: ' 20px'
				}}
				onClick={this.props.handleBack}
				bsStyle=""
				className="btn btn-custom"
				id="backButton"
			>
				<Glyphicon glyph="arrow-left" />
			</Button>
		);
	}
}

BackButton.propTypes = {
	handleBack: PropTypes.func
};

export default BackButton;
