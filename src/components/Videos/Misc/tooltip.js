import React from 'react';
import { Tooltip } from 'react-bootstrap';

const tooltip = (
	<Tooltip id="tooltip" bsClass="tooltip">
		Remove this video from the Highlight
	</Tooltip>
);

export default tooltip;
