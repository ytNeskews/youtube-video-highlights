import React, { Component } from 'react';
import PropTypes from 'prop-types';

import STYLES from '../styles';

class VideoPanel extends Component {
	render() {
		return <div style={STYLES.videoPanel}>{this.props.children}</div>;
	}
}

VideoPanel.propTypes = {
	children: PropTypes.arr
};

export default VideoPanel;
