import React from 'react';

import ErrorAlerter from '../Hints/ErrorAlerter';
import Banner from '../Misc/Banner';
import Player from '../Videos/Player';
import LoadingSpinner from '../Hints/LoadingSpinner';
import ScreenContent from './Content/ScreenContent';

import getHighlightsFromDB from '../../scripts/getHighlightsFromDB';
import addHighlightToDB from '../../scripts/addHighlightToDB';
import { getPlayerByHighlightId, group } from '../../utils';

const LOGIN_STORAGE = window.localStorage;

export default class MainHighlightScreen extends React.PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			selectedHighlight: undefined
		};

		this.handleHighlightSelect = this.handleHighlightSelect.bind(this);
		this.handleBack = this.handleBack.bind(this);
		this.handleHighlightAdd = this.handleHighlightAdd.bind(this);
	}

	handleHighlightSelect(event) {
		this.setState({ selectedHighlight: event.target.value });
	}

	async handleHighlightAdd() {
		try {
			const idCustomers = LOGIN_STORAGE.getItem('idCustomers');

			// await the response we get when adding a highlight to DB
			const response = await addHighlightToDB({ idCustomers });

			// if we don't have a statusCode of 200, trigger an error message to show up
			if (response.statusCode !== 200)
				this.setState({ errorMessage: 'Could not add a new Highlight' });

			// get the updated highlights from DB
			const highlights = await getHighlightsFromDB();

			// put the highlights we get from DB into the needed form and update the state
			this.setState({ highlights: group({ highlights }) });
		} catch (error) {
			if (error) this.setState({ errorMessage: error.message });
		}
	}

	async handleBack() {
		// if we want to go back to overview screen, set the selected highlight to undefined
		this.setState({ selectedHighlight: undefined });
	}

	async componentDidMount() {
		try {
			// ass soon as the component mounts, we await the highlights from DB
			const highlights = await getHighlightsFromDB();

			// put the highlights into the form we need and update the state
			this.setState({ highlights: group({ highlights }) });
		} catch (error) {
			// if we catch an error, trigger an error message to show up
			this.setState({ errorMessage: error.message });
		}
	}

	render() {
		const player =
			this.state.selectedHighlight &&
			getPlayerByHighlightId({
				selectedHighlight: parseInt(this.state.selectedHighlight, 10),
				highlights: this.state.highlights
			});

		return (
			<div>
				<Banner />

				{!this.state.highlights &&
					!this.state.errorMessage && <LoadingSpinner />}

				{this.state.errorMessage && (
					<ErrorAlerter errorMessage={this.state.errorMessage} />
				)}

				{// in case we have selected an highlight
				this.state.selectedHighlight && (
					<Player
						videoIds={player.videoIds}
						idCustomers={player.idCustomers}
						highlightId={parseInt(this.state.selectedHighlight, 10)}
						handleBack={this.handleBack}
					/>
				)}

				{this.state.highlights &&
					!this.state.selectedHighlight && (
						<ScreenContent
							handleHighlightSelect={this.handleHighlightSelect}
							handleHighlightAdd={this.handleHighlightAdd}
							highlights={this.state.highlights}
						/>
					)}
			</div>
		);
	}
}
