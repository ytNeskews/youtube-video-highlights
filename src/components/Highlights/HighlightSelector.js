import React from 'react';
import PropTypes from 'prop-types';
import { Button, Glyphicon } from 'react-bootstrap';

import HighlightPanel from '../Panels/HighlightPanel';

export default class HighlightSelector extends React.PureComponent {
	render() {
		return (
			<HighlightPanel>
				<h1>Highlight</h1>
				<p>A playlist of your favourite videos</p>
				<p>
					<Button
						bsStyle=""
						className="highlight"
						onClick={this.props.handleHighlightSelect}
						value={this.props.highlightId}
					>
						<Glyphicon glyph="play" /> Go to Highlight
					</Button>
				</p>
			</HighlightPanel>
		);
	}
}

HighlightSelector.propTypes = {
	handleHighlightSelect: PropTypes.func,
	highlightId: PropTypes.number
};
