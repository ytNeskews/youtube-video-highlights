import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Modal, Button } from 'react-bootstrap';

import HighlightSelector from '../HighlightSelector';
import HighlightAddButton from '../HighlightAddButton';

import LoadingSpinner from '../../Hints/LoadingSpinner';

const LOGIN_STORAGE = window.localStorage;
const name = LOGIN_STORAGE.getItem('name');

class ScreenContent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			show: true
		};

		this.close = this.close.bind(this);
	}

	close() {
		window.localStorage.setItem('closedAppDesc', true);
		this.setState({
			show: false
		});
	}

	render() {
		const { highlights } = this.props;
		return (
			<div>
				{!window.localStorage.getItem('closedAppDesc') && (
					<Modal
						{...this.props}
						show={this.state.show}
						onHide={this.handleHide}
						dialogClassName="custom-modal"
						bsSize="lg"
					>
						<Modal.Header>
							<Modal.Title id="contained-modal-title-lg">
								Collaborate with all the famouse YouTubers!
							</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<ul
								style={{
									fontSize: '16px'
								}}
							>
								<li>
									This app increases the views and watch time of your videos by
									connecting them with those of famouse YouTubers.
								</li>
								<li>
									Create awesome playlists with your videos, the ones of your
									friends and all other YouTubers.
								</li>
							</ul>
							<div
								style={{
									padding: '100px 0px 100px 0px',
									backgroundColor: 'rgba(20,20,20,0.95)',
									borderRadius: '2px',
									color: 'white',
									fontSize: '24px',
									textAlign: 'center'
								}}
							>
								Become a renowned YouTuber!
							</div>
						</Modal.Body>
						<Modal.Footer>
							<Button onClick={this.close}>Close</Button>
						</Modal.Footer>
					</Modal>
				)}

				{name && (
					<HighlightAddButton
						handleHighlightAdd={this.props.handleHighlightAdd}
					/>
				)}

				{highlights ? (
					highlights.map(highlight => (
						<HighlightSelector
							key={highlight.idHighlight}
							handleHighlightSelect={this.props.handleHighlightSelect}
							highlightId={highlight.highlightId}
						/>
					))
				) : (
					<LoadingSpinner />
				)}
			</div>
		);
	}
}

ScreenContent.propTypes = {
	highlights: PropTypes.array,
	handleHighlightAdd: PropTypes.func,
	handleHighlightSelect: PropTypes.func
};

export default ScreenContent;
