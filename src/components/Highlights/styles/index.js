const STYLES = {
	backgroundColor: 'rgba(40,40,40,0.85)',
	color: 'white',
	width: '68%',
	fontSize: '14px',
	marginRight: '30%',
	marginBottom: '15px',
	marginLeft: '2%',
	marginTop: '20px'
};

export default STYLES;
