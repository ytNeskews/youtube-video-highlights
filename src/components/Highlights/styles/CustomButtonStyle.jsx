import React, { Component } from 'react';

class CustomButtonStyle extends Component {
	render() {
		return (
			<style type="text/css">{`
      .custom {
          background-color: rgba(40,40,40,0.85);
          color: white;
          width: 68%;
          font-size: 14px;
          marginRight: 30%;
          margin-bottom: 15px;
          margin-left: 2%;
          margin-top: 20px;
      }

      .highlight {
          background-color: white;
          border-radius: 5px;
          color: black;
          font-size: 14px;
      }
      `}</style>
		);
	}
}

export default CustomButtonStyle;
