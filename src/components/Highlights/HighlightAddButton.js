import React, { Component } from 'react';
import { Button, Glyphicon, OverlayTrigger, Tooltip } from 'react-bootstrap';
import PropTypes from 'prop-types';

import CustomButtonStyle from './styles/CustomButtonStyle';

const TOOLTIP = <Tooltip id="tooltip">Add a new Highlight!</Tooltip>;

/**
 * This Error Alerter should be used to show up error messages in the GUI
 */
class HighlightAddButton extends Component {
	render() {
		return (
			<OverlayTrigger placement="bottom" overlay={TOOLTIP}>
				<div>
					<CustomButtonStyle />
					<Button
						bsStyle=""
						className="custom"
						onClick={this.props.handleHighlightAdd}
					>
						<Glyphicon glyph="plus" />
					</Button>
				</div>
			</OverlayTrigger>
		);
	}
}

HighlightAddButton.propTypes = {
	handleHighlightAdd: PropTypes.func
};

export default HighlightAddButton;
