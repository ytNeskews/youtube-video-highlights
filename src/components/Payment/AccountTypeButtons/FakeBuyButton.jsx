import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Glyphicon } from 'react-bootstrap';

import STYLES from './styles';

class FakeBuyButton extends Component {
	render() {
		return (
			<Button
				bsStyle=""
				style={STYLES.button}
				onClick={this.props.createAccount}
			>
				<Glyphicon glyph="euro" style={{ marginRight: '10px' }} />Fake Buy
			</Button>
		);
	}
}

FakeBuyButton.propTypes = {
	createAccount: PropTypes.func
};

export default FakeBuyButton;
