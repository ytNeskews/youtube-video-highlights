const LOGIN_STORAGE = window.localStorage;

export function handleLogoutClick() {
	LOGIN_STORAGE.clear();
	window.location.reload();
}

export default { handleLogoutClick };
