import React, { Component } from 'react';
import { Button, Glyphicon } from 'react-bootstrap';

import { handleLogoutClick } from './utils/clicks';

import STYLES from './styles';

class Basic extends Component {
	render() {
		return (
			<Button bsStyle="" style={STYLES.button} onClick={handleLogoutClick}>
				<Glyphicon glyph="off" style={{ marginRight: '10px' }} />Logout
			</Button>
		);
	}
}

export default Basic;
