const STYLES = {
	table: {
		fontSize: '14px'
	},
	button: {
		fontSize: '14px',
		borderRadius: '5px',
		backgroundColor: 'rgba(20,20,20,0.9)',
		color: '#ffffff',
		marginTop: '10px',
		marginRight: '5px',
		height: '40px',
		textAlign: 'center'
	},
	buttonRules: {
		fontSize: '10px',
		borderRadius: '5px',
		backgroundColor: 'rgba(20,20,20,0.9)',
		color: '#ffffff',
		marginTop: '10px',
		marginRight: '5px',
		height: '30px',
		textAlign: 'center'
	}
};

export default STYLES;
