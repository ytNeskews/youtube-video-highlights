import React, { Component } from 'react';
import PaypalExpressBtn from 'react-paypal-express-checkout';
import PropTypes from 'prop-types';

class PayButton extends Component {
	render() {
		let env = 'sandbox'; // you can set here to 'production' for production
		let currency = 'EUR'; // or you can set this value from your props or state
		let total = this.props.toPay; // same as above, this is the total amount (based on currency) to be paid by using Paypal express checkout
		// Document on Paypal's currency code: https://developer.paypal.com/docs/classic/api/currency_codes/

		const client = {
			sandbox:
				'AZa8kRY9mwk8_yiVu5KB8456mu6qgSglK5DyrRhkMS1H0Ca7-d0uqKK9YTk4yxzbatw6meNF2Ywqzi4p',
			production: 'YOUR-PRODUCTION-APP-ID'
		};
		// In order to get production's app-ID, you will have to send your app to Paypal for approval first
		// For sandbox app-ID (after logging into your developer account, please locate the "REST API apps" section, click "Create App"):
		//   => https://developer.paypal.com/docs/classic/lifecycle/sb_credentials/
		// For production app-ID:
		//   => https://developer.paypal.com/docs/classic/lifecycle/goingLive/

		// NB. You can also have many Paypal express checkout buttons on page, just pass in the correct amount and they will work!
		return (
			<PaypalExpressBtn
				env={env}
				client={client}
				currency={currency}
				total={total}
				onError={this.props.onError}
				onSuccess={this.props.onSuccess}
				onCancel={this.props.onCancel}
			/>
		);
	}
}

PayButton.propTypes = {
	toPay: PropTypes.number
};

export default PayButton;
