import React, { Component } from 'react';
import LoginForm from '../Login/LoginForm';
import LoggedInAs from './LoggedInAs';
import PaymentPanel from '../Panels/PaymentPanel';
import Infos from './PaymentDialog/Infos';
import AGB from './Rules/AGB';
import Impress from './Rules/Impress';

const LOGIN_STORAGE = window.localStorage;

const ActualBanner = () => (
	<PaymentPanel>
		<h3>Login</h3>
		<LoginForm />
		<Infos />
		<div style={{ display: 'flex' }}>
			<AGB />
			<Impress />
		</div>
	</PaymentPanel>
);

class GetAccountBanner extends Component {
	render() {
		const name = LOGIN_STORAGE.getItem('name');
		return name ? <LoggedInAs /> : <ActualBanner />;
	}
}

export default GetAccountBanner;
