import React, { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';

import STYLES from '../AccountTypeButtons/styles';

class Impress extends Component {
	constructor(props, context) {
		super(props, context);

		this.handleShow = this.handleShow.bind(this);
		this.handleClose = this.handleClose.bind(this);

		this.state = {
			show: false
		};
	}

	handleClose() {
		this.setState({ show: false });
	}

	handleShow() {
		this.setState({ show: true });
	}
	render() {
		return (
			<div>
				<Button bsStyle="" style={STYLES.buttonRules} onClick={this.handleShow}>
					Impressum & Kontakt
				</Button>
				<Modal
					bsSize="large"
					show={this.state.show}
					style={{ paddingTop: '15%' }}
				>
					<Modal.Body>
						<h3>Impressum & Kontakt</h3>
						<p>
							<b>Name and Adress:</b> YouToo GmbH Girbelsratherstraße 62, 52351
							Düren
						</p>
						<p>
							<b>E-Mail:</b> lh@youtoo.com
						</p>
						<p>
							<b>CEO:</b> Tunahan Yildiz
						</p>
						<p>
							<b>Registered Office:</b> Düren
						</p>
						<p>
							<b>Inhaltlich verantwortlich: </b>Lukas Höpfner
						</p>{' '}
						<p>
							The information contained in this website is for general
							information purposes only. The information is provided by YouToo
							GmbH and while we endeavour to keep the information up to date and
							correct, we make no representations or warranties of any kind,
							express or implied, about the completeness, accuracy, reliability,
							suitability or availability with respect to the website or the
							information, products, services, or related graphics contained on
							the website for any purpose. Any reliance you place on such
							information is therefore strictly at your own risk. In no event
							will we be liable for any loss or damage including without
							limitation, indirect or consequential loss or damage, or any loss
							or damage whatsoever arising from loss of data or profits arising
							out of, or in connection with, the use of this website. Through
							this website you are able to link to other websites which are not
							under the control of YouToo GmbH. We have no control over the
							nature, content and availability of those sites. The inclusion of
							any links does not necessarily imply a recommendation or endorse
							the views expressed within them. Every effort is made to keep the
							website up and running smoothly. However, YouToo GmbH takes no
							responsibility for, and will not be liable for, the website being
							temporarily unavailable due to technical issues beyond our
							control.
						</p>
					</Modal.Body>
					<Modal.Footer>
						<Modal.Footer>
							<Button onClick={this.handleClose}>Close</Button>
						</Modal.Footer>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}

export default Impress;
