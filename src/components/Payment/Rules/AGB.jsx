import React, { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';

import STYLES from '../AccountTypeButtons/styles';

class AGB extends Component {
	constructor(props, context) {
		super(props, context);

		this.handleShow = this.handleShow.bind(this);
		this.handleClose = this.handleClose.bind(this);

		this.state = {
			show: false
		};
	}

	handleClose() {
		this.setState({ show: false });
	}

	handleShow() {
		this.setState({ show: true });
	}

	render() {
		return (
			<div>
				<Button bsStyle="" style={STYLES.buttonRules} onClick={this.handleShow}>
					AGB
				</Button>
				<Modal
					className="dialog"
					show={this.state.show}
					bsSize="large"
					style={{ paddingTop: '32%' }}
				>
					<div>
						<h3>Vertragspartner</h3>

						<p>
							Auf Grundlage dieser Allgemeinen Geschäftsbedingungen (AGB) kommt
							zwischen dem Kunden und
						</p>
						<ul>
							<li>YouToo GmbH</li>
							<li>Vertreten durch Lukas Höpfner</li>
							<li>Adresse: Girbelsratherstraße 62 52351 Düren</li>
							<li>Tel: 01742930192</li>
							<li>E-Mail-Adresse: lh@youtoo.com</li>
						</ul>

						<p>
							Handelsregister: [Handelsregister] Handelsregisternummer:
							[Handelsregisternummer] Umsatzsteuer-Identifikationsnummer:
							[Umsatzsteuer-Identifikationsnummer], nachfolgend Anbieter
							genannt, der Vertrag zustande. Vertragsgegenstand
						</p>

						<p>
							Durch diesen Vertrag wird der Verkauf von Dienstleistungen aus dem
							Bereich/den Bereichen Entertainment über den Online-Shop des
							Anbieters geregelt. Wegen der Details des jeweiligen Angebotes
							wird auf die Produktbeschreibung der Angebotsseite verwiesen.
							Vertragsschluss
						</p>

						<p>
							Der Vertrag kommt ausschließlich im elektronischen
							Geschäftsverkehr über das Shop-System zustande. Dabei stellen die
							dargestellten Angebote eine unverbindliche Aufforderung zur Abgabe
							eines Angebots durch die Kundenbestellung dar, das der Anbieter
							dann annehmen kann. Der Bestellvorgang zum Vertragsschluss umfasst
							im Shop-System folgende Schritte:
						</p>

						<ul>
							<li>
								{' '}
								Auswahl des Angebots in der gewünschten Spezifikation (Basic,
								Premium)
							</li>
							<li>Betätigen des Buttons „bezahlen“ per Paypal</li>
							<li>Zahlung per Paypal</li>
							<li>Bestätigungsmail, dass Bestellung eingegangen ist</li>
						</ul>

						<p>
							Mit der Zusendung der Bestellbestätigung kommt der Vertrag
							zustande.
						</p>

						<h3>Vertragsdauer</h3>

						<p>
							Der Vertrag hat wiederkehrende/dauernde Leistungen zum Gegenstand.
							Der Vertrag hat vorbehaltlich einer Kündigung eine Laufzeit von 1
							Monate. Der Gesamtpreis errechnet sich aus den folgenden
							Komponenten: 8,33 Premium 3,57 Basic Jede Vertragspartei hat das
							Recht den Vertrag mit einer Frist von 1 Monate zum Monatsende ohne
							Angabe von Gründen zu kündigen. Das Recht auf außerordentliche
							Kündigung aus wichtigem Grund, insbesondere dem wiederholten
							Verstoß gegen die vertraglichen Hauptpflichten bleibt unberührt.
							Die Kündigung ist nur wirksam, wenn sie in folgender Form erfolgt:
							Elektronisch/E-Mail. Preise, Versandkosten, Rücksendekosten
						</p>

						<p>
							Alle Preise sind Endpreise und enthalten gem. § 19 Abs. 1 UStG
							keine Umsatzsteuer (Mehrwertsteuer). Neben den Endpreisen fallen
							je nach Versandart weitere Kosten an, die vor Versendung der
							Bestellung angezeigt werden. Besteht ein Widerrufsrecht und wird
							von diesem Gebraucht gemacht, trägt der Kunde die Kosten der
							Rücksendung.
						</p>

						<h3>Zahlungsbedingungen</h3>

						<p>
							Der Kunde hat ausschließlich folgende Möglichkeiten zur Zahlung:
							Zahlungsdienstleister (PayPal). Weitere Zahlungsarten werden nicht
							angeboten und werden zurückgewiesen. Bei Verwendung eines
							Treuhandservice/ Zahlungsdienstleisters ermöglicht es dieser dem
							Anbieter und Kunden, die Zahlung untereinander abzuwickeln. Dabei
							leitet der Treuhandservice/ Zahlungsdienstleister die Zahlung des
							Kunden an den Anbieter weiter. Weitere Informationen erhalten Sie
							auf der Internetseite des jeweiligen Treuhandservices/
							Zahlungsdienstleisters. Der Kunde ist verpflichtet innerhalb von 1
							Tagen nach Erhalt der Rechnung den ausgewiesenen Betrag auf das
							auf der Rechnung angegebene Konto einzuzahlen oder zu überweisen.
							Die Zahlung ist ab Rechnungsdatum ohne Abzug fällig. Der Kunde
							kommt erst nach Mahnung in Verzug.
						</p>

						<h3>Lieferbedingungen</h3>

						<p>
							Die Ware wird umgehend nach Eingang der Bestellung versandt. Der
							Versand erfolgt durchschnittlich spätestens nach 1 Tagen. Der
							Unternehmer verpflichtet sich zur Lieferung am 1 Tag nach
							Bestelleingang. Die Regellieferzeit beträgt 1 Tage, wenn in der
							Artikelbeschreibung nichts anderes angegeben ist. Der Anbieter
							versendet die Bestellung aus eigenem Lager, sobald die gesamte
							Bestellung dort vorrätig ist. Vertragsgestaltung
						</p>

						<p>
							Der Kunde hat keine Möglichkeit selbst direkt auf den
							gespeicherten Vertragstext zuzugreifen. Widerrufsrecht und
							Kundendienst
						</p>

						<h3>Widerrufsbelehrung</h3>

						<h4>Widerrufsrecht</h4>
						<b>
							<p>
								Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von
								Gründen diesen Vertrag zu widerrufen.
							</p>

							<p>Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag,</p>

							<ul>
								<li>
									Im Falle eines Kaufvertrags: an dem Sie oder ein von Ihnen
									benannter Dritter, der nicht der Beförderer ist, die letzte
									Ware in Besitz genommen haben bzw. hat.
								</li>
								<li>
									Im Falle einer Vertrags über mehrere Waren, die der
									Verbraucher im Rahmen einer einheitlichen Bestellung bestellt
									hat und die getrennt geliefert werden: an dem Sie oder ein von
									Ihnen benannter Dritter, der nicht Beförderer ist, die letzte
									Ware in Besitz genommen haben bzw. hat.
								</li>
								<li>
									Im Falle eines Vertrags über die Lieferung einer Ware in
									mehreren Teilsendungen oder Stücken: an dem Sie oder ein von
									Ihnen benannter Dritter, der nicht Beförderer ist, die letzte
									Teilsendung oder das letzte Stück in Besitz genommen haben
									bzw. hat.
								</li>
								<li>
									Im Falle eines Vertrages zur regelmäßigen Lieferung von Waren
									über einen festgelegten Zeitraum hinweg: an dem Sie oder ein
									von Ihnen benannter Dritter, der nicht Beförderer ist, die
									erste Ware in Besitz genommen haben bzw. hat.
								</li>
							</ul>

							<p>
								Beim Zusammentreffen mehrerer Alternativen ist der jeweils
								letzte Zeitpunkt maßgeblich.
							</p>

							<p>
								Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (YouToo GmbH,
								Lukas Höpfner, Girbelsratherstraße 62 52351 Düren 01742930192
								lh@youtoo.com) mittels einer eindeutigen Erklärung (z.B. ein mit
								der Post versandter Brief, Telefax, oder E-Mail) über Ihren
								Entschluss, diesen Vertrag zu widerrufen, informieren. Sie
								können dafür das beigefügte Muster-Widerrufsformular verwenden,
								das jedoch nicht vorgeschrieben ist.
							</p>

							<p>
								Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die
								Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der
								Widerrufsfrist absenden.
							</p>

							<h4>Folgen des Widerrufs</h4>

							<p>
								Wenn Sie diesen Vertag widerrufen, haben wir Ihnen alle
								Zahlungen, die wir von Ihnen erhalten haben, einschließlich der
								Lieferkosten (mit Ausnahmen der zusätzlichen Kosten, die sich
								daraus ergeben, dass Sie einer andere Art der Lieferung als die
								von uns angebotene, günstige Standardlieferung gewählt haben),
								unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag
								zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses
								Vertrags bei uns eingegangen ist. Für diese Rückzahlung
								verwenden wir dasselbe Zahlungsmittel, das Sie bei der
								ursprünglichen Transaktion eingesetzt haben, es sei denn, mit
								Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem
								Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet.
								Wir können die Rückzahlung verweigern, bis wir die Waren wieder
								zurückerhalten haben oder bis Sie den Nachweis erbracht haben,
								dass Sie die Waren zurückgesandt haben, je nachdem, welches der
								frühere Zeitpunkt ist.
							</p>

							<p>
								Sie haben die Waren unverzüglich und in jedem Fall spätestens
								binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den
								Widerruf dieses Vertrags unterrichten, an YouToo GmbH, Lukas
								Höpfner, Girbelsratherstraße 62 52351 Düren 01742930192
								lh@youtoo.com uns zurückzusenden oder zu übergeben. Die Frist
								ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von
								vierzehn Tagen absenden.
							</p>

							<p>
								Sie tragen die unmittelbaren Kosten der Rücksendung der Waren.
							</p>

							<p>
								Sie müssen für einen etwaigen Wertverlust der Waren nur
								aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der
								Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht
								notwendigen Umgang mit ihnen zurückzuführen ist.
							</p>

							<h4>Ende der Widerrufsbelehrung</h4>
						</b>
						<h3>Sprache, Gerichtsstand und anzuwendendes Recht</h3>

						<p>
							Der Vertrag wird in Englisch, Deutsch abgefasst. Die weitere
							Durchführung der Vertragsbeziehung erfolgt in Englisch, Deutsch.
							Es findet ausschließlich das Recht der Bundesrepublik Deutschland
							Anwendung. Für Verbraucher gilt dies nur insoweit, als dadurch
							keine gesetzlichen Bestimmungen des Staates eingeschränkt werden,
							in dem der Kunde seinen Wohnsitz oder gewöhnlichen Aufenthalt hat.
						</p>

						<h3>Datenschutz</h3>

						<p>
							Im Zusammenhang mit der Anbahnung, Abschluss, Abwicklung und
							Rückabwicklung eines Kaufvertrages auf Grundlage dieser AGB werden
							vom Anbieter Daten erhoben, gespeichert und verarbeitet. Dies
							geschieht im Rahmen der gesetzlichen Bestimmungen. Der Anbieter
							gibt keine personenbezogenen Daten des Kunden an Dritte weiter, es
							sei denn, dass er hierzu gesetzlich verpflichtet wäre oder der
							Kunde vorher ausdrücklich eingewilligt hat. Wird ein Dritter für
							Dienstleistungen im Zusammenhang mit der Abwicklung von
							Verarbeitungsprozessen eingesetzt, so werden die Bestimmungen des
							Bundesdatenschutzgesetzes eingehalten. Die vom Kunden im Wege der
							Bestellung mitgeteilten Daten werden ausschließlich zur
							Kontaktaufnahme innerhalb des Rahmens der Vertragsabwicklung und
							nur zu dem Zweck verarbeitet, zu dem der Kunde die Daten zur
							Verfügung gestellt hat. Die Daten werden nur soweit notwendig an
							das Versandunternehmen, das die Lieferung der Ware auftragsgemäß
							übernimmt, weitergegeben. Die Zahlungsdaten werden an das mit der
							Zahlung beauftragte Kreditinstitut weitergegeben. Soweit den
							Anbieter Aufbewahrungsfristen handels- oder steuerrechtlicher
							Natur treffen, kann die Speicherung einiger Daten bis zu zehn
							Jahre dauern. Während des Besuchs im Internet-Shop des Anbieters
							werden anonymisierte Daten, die keine Rückschlüssen auf
							personenbezogene Daten zulassen und auch nicht beabsichtigen,
							insbesondere IP-Adresse, Datum, Uhrzeit, Browsertyp,
							Betriebssystem und besuchte Seiten, protokolliert. Auf Wunsch des
							Kunden werden im Rahmen der gesetzlichen Bestimmungen die
							personenbezogenen Daten gelöscht, korrigiert oder gesperrt. Eine
							unentgeltliche Auskunft über alle personenbezogenen Daten des
							Kunden ist möglich. Für Fragen und Anträge auf Löschung, Korrektur
							oder Sperrung personenbezogener Daten sowie Erhebung, Verarbeitung
							und Nutzung kann sich der Kunde an folgende Adresse wenden: YouToo
							GmbH, Lukas Höpfner, Girbelsratherstraße 62 52351 Düren
							01742930192 lh@youtoo.com.
						</p>
					</div>
					<Modal.Footer>
						<Modal.Footer>
							<Button onClick={this.handleClose}>Close</Button>
						</Modal.Footer>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}

export default AGB;
