import React, { Component } from 'react';

import PaymentPanel from '../Panels/PaymentPanel';
import LogoutButton from './AccountTypeButtons/LogoutButton';
import AGB from './Rules/AGB';
import Impress from './Rules/Impress';

const LOGIN_STORAGE = window.localStorage;

class LoggedInAs extends Component {
	render() {
		const name = LOGIN_STORAGE.getItem('name');
		return (
			<PaymentPanel>
				<div>
					<p>Welcome {name}!</p>
					<LogoutButton />
					<div style={{ display: 'flex', marginTop: '50px' }}>
						<AGB />
						<Impress />
					</div>
				</div>
			</PaymentPanel>
		);
	}
}

export default LoggedInAs;
