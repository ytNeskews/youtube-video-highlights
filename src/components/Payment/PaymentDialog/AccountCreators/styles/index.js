const STYLES = {
	button: {
		fontSize: '14px',
		borderRadius: '5px',
		backgroundColor: 'rgba(20,20,20,0.9)',
		color: '#ffffff',
		marginTop: '5px',
		height: '40px',
		textAlign: 'center'
	}
};

export default STYLES;
