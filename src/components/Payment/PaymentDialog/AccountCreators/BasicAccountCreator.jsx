import React, { Component } from 'react';
import { FormGroup, FormControl, Button } from 'react-bootstrap';

import STYLES from './styles';

/* eslint-disable react/prop-types */

class BasicAccountCreator extends Component {
	render() {
		return (
			<div>
				<FormGroup>
					<FormControl
						type="text"
						value={this.props.accountName}
						placeholder="Your account name"
						onChange={this.props.handleUpdate}
						style={{ height: '50px' }}
					/>
				</FormGroup>
				<Button
					bsStyle=""
					style={STYLES.button}
					onClick={this.props.createBasicAccount}
				>
					Create Account
				</Button>
			</div>
		);
	}
}

export default BasicAccountCreator;
