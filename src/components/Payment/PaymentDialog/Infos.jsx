import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';

import BuyAccountButton from '../../Login/BuyAccountButton';
import GetAccountHandler from './GetAccountHandler';
import BasicPanel from '../../Panels/BasicPanel';

class Infos extends Component {
	constructor(props, context) {
		super(props, context);

		this.handleShow = this.handleShow.bind(this);
		this.handleClose = this.handleClose.bind(this);

		this.state = {
			showInfos: false
		};
	}

	handleClose() {
		this.setState({ showInfos: false });
	}

	handleShow() {
		this.setState({ showInfos: true });
	}

	render() {
		return (
			<div>
				<BuyAccountButton handleShow={this.handleShow} />

				<Modal
					show={this.state.showInfos}
					onHide={this.handleClose}
					bsSize="lg"
					aria-labelledby="contained-modal-title-lg"
					style={{
						marginTop: '7%',
						paddingTop: '10%'
					}}
				>
					<Modal.Header>
						<Modal.Title>
							<BasicPanel>Get your account now!</BasicPanel>
						</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<GetAccountHandler />
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={this.handleClose}>Close</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}

export default Infos;
