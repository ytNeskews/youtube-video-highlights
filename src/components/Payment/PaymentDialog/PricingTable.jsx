import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

import ErrorAlerter from '../../Hints/ErrorAlerter';
import PayButton from '../AccountTypeButtons/PayButton';

import STYLES from '../AccountTypeButtons/styles';

/* eslint-disable react/prop-types */

class PricingTable extends Component {
	onSuccess(payment) {
		// Congratulation, it came here means everything's fine!
		console.log('The payment was succeeded!', payment);
		// You can bind the "payment" object's value to your state or props or whatever here, please see below for sample returned data
	}

	onCancel(data) {
		// User pressed "cancel" or close Paypal's popup!
		console.log('The payment was cancelled!', data);
		// You can bind the "data" object's value to your state or props or whatever here, please see below for sample returned data
	}

	onError(err) {
		// The main Paypal's script cannot be loaded or somethings block the loading of that script!
		console.log('Error!', err);
		// Because the Paypal's main script is loaded asynchronously from "https://www.paypalobjects.com/api/checkout.js"
		// => sometimes it may take about 0.5 second for everything to get set, or for the button to appear
	}

	render() {
		const showPricing = !this.props.successMessage && !this.props.errorMessage;
		const showError = !!this.props.errorMessage;

		return (
			<div>
				{showError && <ErrorAlerter errorMessage={this.props.errorMessage} />}

				{showPricing && (
					<div>
						<Table style={STYLES.table}>
							<thead>
								<tr>
									<th>Article</th>
									<th>Price</th>
									<th>Feature</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Free</td>
									<td>No costs</td>
									<td>Add 1 Highlight per day</td>
									<td>Simply enjoy the App!</td>
								</tr>
								<tr>
									<td>Basic</td>
									<td>3,57€ per month</td>
									<td>Create Playlists and add 3 videos per day</td>
									<td style={{ display: 'flex' }}>
										<PayButton
											toPay={3.57}
											onSuccess={this.props.createBasicAccount}
											onCancel={this.onCancel}
											onError={this.onError}
										/>
									</td>
								</tr>
								<tr>
									<td>Premium</td>
									<td>8,33€ per month</td>
									<td>
										Create Playlists, add 6 videos per day and choose user
										rights
									</td>
									<td style={{ display: 'flex' }}>
										<PayButton
											toPay={8.33}
											onSuccess={this.props.createPremiumAccount}
											onCancel={this.onCancel}
											onError={this.onError}
										/>
									</td>
								</tr>
							</tbody>
						</Table>
					</div>
				)}
			</div>
		);
	}
}

export default PricingTable;
