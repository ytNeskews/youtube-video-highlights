import React, { Component } from 'react';

import PricingTable from './PricingTable';
import SuccessHint from '../../Hints/SuccessHint';
import ErrorAlerter from '../../Hints/ErrorAlerter';

import BasicAccountCreator from './AccountCreators/BasicAccountCreator';
import PremiumAccountCreator from './AccountCreators/PremiumAccountCreator';

import buyPremium from './buyActions/buyPremium';
import buyBasic from './buyActions/buyBasic';

class GetAccountHandler extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fakeMode: false,
			errorMessage: null,
			successMessage: null,
			name: '',
			newAccountName: '',
			creatorToShow: 'none' // one of [ 'none', 'basic', 'premiun', 'success', 'error' ]
		};

		this.handleUpdate = this.handleUpdate.bind(this);
		this.createBasicAccount = this.createBasicAccount.bind(this);
		this.forwardCreateBasicAccount = this.forwardCreateBasicAccount.bind(this);

		this.createPremiumAccount = this.createPremiumAccount.bind(this);
		this.forwardCreatePremiumAccount = this.forwardCreatePremiumAccount.bind(
			this
		);
	}

	handleUpdate(event) {
		const { value } = event.target;
		this.setState({
			newAccountName: value
		});
	}

	handleError(error) {
		console.log(error);
		this.setState({
			errorMessage:
				'Could not create an account. You will not be charged for it.'
		});
	}

	forwardCreateBasicAccount() {
		this.setState({
			creatorToShow: 'basic'
		});
	}

	forwardCreatePremiumAccount() {
		this.setState({
			creatorToShow: 'premium'
		});
	}

	async createBasicAccount() {
		try {
			const { newAccountName } = this.state;

			const response = await buyBasic({ name: newAccountName });
			const { message, errno } = response.body;

			if (errno) {
				this.setState({
					errorMessage: 'Could not create the premium account',
					creatorToShow: 'error'
				});
			} else {
				this.setState({
					successMessage: message,
					creatorToShow: 'success'
				});
			}
		} catch (error) {
			this.setState({
				errorMessage: error.message,
				creatorToShow: 'error'
			});
		}
	}

	async createPremiumAccount() {
		try {
			const { newAccountName } = this.state;

			const response = await buyPremium({ name: newAccountName });
			const { message, errno } = response.body;

			if (errno) {
				this.setState({
					errorMessage: 'Could not create the premium account',
					creatorToShow: 'error'
				});
			} else {
				this.setState({
					successMessage: message,
					creatorToShow: 'success'
				});
			}
		} catch (error) {
			this.setState({
				errorMessage: error.message,
				creatorToShow: 'error'
			});
		}
	}

	render() {
		return (
			<div>
				{this.state.creatorToShow === 'basic' && (
					<BasicAccountCreator
						createBasicAccount={this.createBasicAccount}
						handleUpdate={this.handleUpdate}
					/>
				)}
				{this.state.creatorToShow === 'premium' && (
					<PremiumAccountCreator
						createPremiumAccount={this.createPremiumAccount}
						handleUpdate={this.handleUpdate}
					/>
				)}
				{this.state.creatorToShow === 'none' && (
					<PricingTable
						successMessage={this.state.successMessage}
						onError={this.handleError}
						errorMessage={this.state.errorMessage}
						toggleFakeMode={this.toggleFakeMode}
						createBasicAccount={this.forwardCreateBasicAccount}
						createPremiumAccount={this.forwardCreatePremiumAccount}
						fakeMode={this.state.fakeMode}
					/>
				)}
				{this.state.creatorToShow === 'success' && (
					<SuccessHint>{this.state.successMessage}</SuccessHint>
				)}
				{this.state.creatorToShow === 'error' && (
					<ErrorAlerter errorMessage={this.state.errorMessage} />
				)}
			</div>
		);
	}
}

export default GetAccountHandler;
