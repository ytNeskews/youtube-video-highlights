import callBuyPremium from '../../../../scripts/server/callBuyPremium';

/**
 * @param { string } name - the name of the customer we want to update
 */
export default async function buyPremium({ name }) {
	const response = await callBuyPremium({ endpoint: 'buyPremium', name });
	return response;
}
