import React, { Component } from 'react';

import STYLES from './styles';

class HighlightPanel extends Component {
	render() {
		/* eslint-disable react/prop-types */
		return <div style={STYLES.main}>{this.props.children}</div>;
	}
}

export default HighlightPanel;
