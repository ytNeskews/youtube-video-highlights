import React, { Component } from 'react';

import STYLES from './styles';

class PaymentPanel extends Component {
	render() {
		/* eslint-disable react/prop-types */
		return <div style={STYLES.payment}>{this.props.children}</div>;
	}
}

export default PaymentPanel;
