import React, { Component } from 'react';

import STYLES from './styles';

class BasicPanel extends Component {
	render() {
		/* eslint-disable react/prop-types */
		return <div style={STYLES.basic}>{this.props.children}</div>;
	}
}

export default BasicPanel;
