const STYLES = {
	main: {
		padding: '10px',
		backgroundColor: 'rgba(40,40,40,0.8)',
		color: 'white',
		borderRadius: '5px',
		paddingLeft: '2%',
		paddingTop: '2%',
		paddingBottom: '2%',
		marginBottom: '20px',
		marginLeft: '2%',
		marginRight: '30%'
	},
	payment: {
		padding: '10px',
		backgroundColor: 'rgba(40,40,40,0.95)',
		color: 'white',
		fontSize: '16px',
		borderRadius: '5px',
		marginLeft: '2%',
		display: 'flex',
		flexDirection: 'column',
		width: '300px',
		height: '100%'
	},
	basic: {
		backgroundColor: 'rgba(40,40,40,0.95)',
		color: 'white',
		fontSize: '24px',
		borderRadius: '5px',
		display: 'flex',
		paddingLeft: '40px',
		paddingRight: '40px'
	}
};

export default STYLES;
